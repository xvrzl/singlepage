import React from 'react';
import './App.css';
import Title from './components/Title';

class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
      numero: 0,
    };
  }


  render(){
    return (
      <section>
        <div>
          <div>
            <Title title="SolvoWeb!"></Title>
            <h2>{this.state.numero}</h2>
            <button onClick={()=>{this.setState({numero: this.state.numero+1})}}> Crear cuenta gratuita </button>
            <img src={process.env.PUBLIC_URL + '/Images/top-background.png'} alt="Solvo" height="300"/>
            <div>
              <ul>
                <li>
                  <h3>Calificaiones con emociones</h3>
                  <p>Califica tus lugares con experiencias, no con números.</p>
                </li>
                <li>
                  <h3>¿Sin Internet? Sin problemas</h3>
                  <p>Solvo funciona sin internet o conexiones lentas.</p>
                </li>
                <li>
                <h3>Tus lugares favoritos</h3>
                <p>Define tu lista de sitios favoritos.</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default App;
